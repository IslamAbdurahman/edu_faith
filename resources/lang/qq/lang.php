<?php

return [
    // main page //
    'title' => 'Sálem',
    'page'=>'Bet',
    'sms_service'=>'SMS xızmeti',
    'email'=>'Email',
    'type_email'=>'Email kiriting',
    'password'=>'Parol',
    'type_password'=>'Parol kiriting',
    'checking'=>'Tekseriw',
    'close'=>'Keyin basıp',
    'save'=>'Saqlaw',
    'main_page'=>'Tiykarǵı bet',
    'lids'=>'Lidlar',
    'days'=>'kunlar',
    'expired'=>'Múddet',
    'search'=>'Qıdırıw',
    'english'=>'Eng',
    'uzbek'=>'Uz',
    'russian'=>'Ru',
    'karakalpak'=>'Qq',
    'page_here'=>'Bet bul jerde',


    // sidebar //

    'dashboard'=>'Basqarıw paneli',
    'profile'=>'Profil',
    'admins'=>'Adminlar',
    'teachers'=>'Oqıtıwshılar',
    'workers'=>'Jumısshılar',
    'edit_profile'=>'Profil redaktorlash',
    'log_out'=>'Shıǵıw',
    'rooms'=>'Bólmeler',
    'sciences'=>'Pánler',
    'students'=>'Oqıwshılar',
    'courses'=>'Kurslar',
    'groups'=>'Gruppalar',
    'graphics'=>'Grafiklar',
    'cashbox'=>'Kassa',
    'payments'=>'Tólewlar',
    'outputs'=>'Chiqimlar',
    'tests'=>'Testlar',
    'developer'=>'Programmist',


    // Dashboard //
    'more_info'=>'Kóbirek ',
    'payment_graphics'=>'Tólew grafigi',
    'calendar'=>'Kalendar',
    'group'=>'Gruppa',
    'paid'=>'Tolıqngan',
    'remaining'=>'Tólenbegen',


    // Admins //
    'name'=>'Ism',
    'phone'=>'Tel nomer',
    'image'=>'Súwret',
    'role'=>'Rol',
    'add'=>'Qosıw',
    'type_name'=>'At kirgiziw',
    'type_phone'=>'Tel nomer kirgiziw',
    'select'=>'Tańlaw',
    'rights'=>'Huqıqlar',
    'admin_update'=>'Admin redaktorlash',
    'delete'=>'Óshiriw',
    'your_profile'=>'Sizdiń profiligiz',
    'read_only'=>'Tek oqıw',
    'update'=>'Jańalaw',
    'delete_message'=>'Isenimińiz kámalmi',


    // Teachers //

    'balance'=>'Balans',
    'science'=>'Fan',

    // room. show //

    'room'=>'Xana',
    'monday' => 'Dúyshembi',
    'tuesday' => 'Shiyshenbi',
    'wednesday' => 'Shárshembi',
    'thursday' => 'Byshenbe',
    'friday' => 'Juma',
    'saturday' => 'Shembi',
    'sunday' => 'Ekshembi',

    'send_sms_all_students'=>'Barlıq studentlerge sms jiberiw',
    'sms'=>'SMS',
    'type_sms'=>'SMS kiriting',
    'send'=>'Jıberiw',
    'gender'=>'Jins',
    'birthday'=>'Tuwılǵan sana',
    'discount_education'=>'Chegirma',
    'parent_phone'=>'Ata-ana telefon nomeri',
    'select_gender'=>'Jinsni saylań',
    'male'=>'Er adam',
    'female'=>'Áyel',
    'type_parent_phone'=>'Ata-ana telefon nomeri kiriting',
    'percent'=>'Protsent',
    'select_group'=>'Gruppanı saylań',
    'select_lid'=>'Lid saylań',
    'comment'=>'Túsindirme',
    'type_comment'=>'Túsindirme kiriting',



    // Groups //

    'a_z'=>'A-z',
    'z_a'=>'Z-a',
    'absent'=>'Y',
    'reason'=>'S',
    'came'=>'B',

    'missed'=>'Qaldırilgan',

    'added_date'=>'Qo`shilgan sana',
  'attendance'=>'Davomat',

  'level'=>'Dáreje',
  'amount'=>'Muǵdar',
  'teacher'=>'Oqıtıwshı',
  'course'=>'Kurslar',
  'start'=>'Baslanıw',
  'end'=>'Tawısıw',
  'type_level'=>'Dáreje kiriting',
  'type_amount'=>'Muǵdar kiriting',
  'not_selected'=>'Tańlanmagan',
  'teacher_percent'=>'Oqıtıwshı procenti',
  'add_graphics'=>'Grafik qosıw',
  'select_year'=>'Jıldı saylań',
  'education'=>'Tálim',
  'kitchen'=>'Asxana',
  'bedroom'=>'Jataqxana',


  // Graphics //

  'group_graphic_title'=>'Bul gruppaǵa SMS jiberiw : ',
  'group_graphic_text'=>'Xabarlar saylanǵan gruppa aǵzalarına jiberiledi',
  'unfinished'=>'Tamamlanmagan',
  'finished'=>'Pıtken',
  'student'=>'Oqıwshı',
  'graph'=>'Grafik',
  'month'=>'Oy',
  'unpaid'=>'Tolanmagan',
  'send_sms'=>'SMS jıberiw',


  // cash box //

  'cash_cashbox'=>'Naqd',
  'card_cashbox'=>'Karta arqalı',
  'click_cashbox'=>'Click arqalı',
  'personal'=>'Jeke',


  // payments //

  'date'=>'Sana',
  'discount'=>'Chegirma',
  'type_discount'=>'Chegirma kirgiziw',
  'worker'=>'Jumısshı',
  'user'=>'Paydalanıwshı',





  // Controllers //

  'saved'=>'Tabıslı saqlandi',
  'updated'=>'Tabıslı jańalandi',
  'deleted'=>'Tabıslı óshirildi',
  'cannot_create'=>'Saqlap bo`lmadi',
  'cannot_update'=>'Jańalap bo`lmadi',
  'cannot_delete'=>'Óshirip bo`lmadi',

  'student_registered'=>'Student dizimnen ótti',

  'all_sms_sent'=>'Barlıq SMS jiberildi. ',

  'sms_sent'=>'SMS jiberildi. ',

  'busy_room'=>'Bólme saylanǵan waqıtta bánt. Gruppanı saqlap bo`lmadi',

  'sms_updated'=>'Sms xızmet jańalandi',
  'sms_invalid'=>'Login yamasa parol nadurıs',


  'limit'=>'Platformaga studentlerdi qosıw maksimal dárejege jetti',


    'date_zone'=>'uz-uz',

    'text'=>'Text',
    'service'=>'Xizmat',
    'status'=>'Holat',

    'test'=>'Test',
    'result'=>'Natija',
    'type_result'=>'Natija kiriting',

    'reason_text'=>'Sababli',
    'absent_text'=>'Yo`q',

    'login_header'=>'Email va parol kiriting',
    'remember'=>'Eslab qolish',
    'login'=>'Kirish',
    'or'=>'YOKI',
    'forget_password'=>'Parolni unutdim',

    'kassa_error'=>'Kassada yetarli mablag` mavjud emas.',
    'teacher_balance_error'=>'O`qituvchi hisobida yetarli mablag` mavjud emas.',

    'daily_timetable'=>'Kunlik dars jadval',

    'upload_excel'=>'Excel faylın saylań',


    'nickname'=>'Nickname',
    'key'=>'Key',
    'secret'=>'Secret',
    'token'=>'Token',









];
