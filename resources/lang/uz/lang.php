<?php

return [
    // main page //
    'title' => 'Salom',
    'page'=>'Sahifa',
    'sms_service'=>'SMS xizmati',
    'email'=>'Email',
    'type_email'=>'Email kiriting',
    'password'=>'Parol',
    'type_password'=>'Parol kiriting',
    'checking'=>'Tekshirish',
    'close'=>'Yopish',
    'save'=>'Saqlash',
    'main_page'=>'Asosiy sahifa',
    'lids'=>'Lidlar',
    'days'=>'kunlar',
    'expired'=>'Muddat',
    'search'=>'Qidirish',
    'english'=>'Eng',
    'uzbek'=>'Uz',
    'russian'=>'Ru',
    'karakalpak'=>'Qq',
    'page_here'=>'Sahifa  bu yerda',


    // sidebar //

    'dashboard'=>'Boshqaruv paneli',
    'profile'=>'Profil',
    'admins'=>'Adminlar',
    'teachers'=>'O`qituvchilar',
    'workers'=>'Ishchilar',
    'edit_profile'=>'Profil taxrirlash',
    'log_out'=>'Chiqish',
    'rooms'=>'Xonalar',
    'sciences'=>'Fanlar',
    'students'=>'O`quvchilar',
    'courses'=>'Kurslar',
    'groups'=>'Guruhlar',
    'graphics'=>'Grafiklar',
    'cashbox'=>'Kassa',
    'payments'=>'To`lovlar',
    'outputs'=>'Chiqimlar',
    'tests'=>'Testlar',
    'developer'=>'Dasturchi',


    // Dashboard //
    'more_info'=>'Ko`proq ',
    'payment_graphics'=>'To`lov grafigi',
    'calendar'=>'Kalendar',
    'group'=>'Guruh',
    'paid'=>'To`langan',
    'remaining'=>'To`lanmagan',


    // Admins //
    'name'=>'Ism',
    'phone'=>'Tel raqam',
    'image'=>'Rasm',
    'role'=>'Rol',
    'add'=>'Qo`shish',
    'type_name'=>'Ism kiritish',
    'type_phone'=>'Tel raqam kiritish',
    'select'=>'Tanlash',
    'rights'=>'Huquqlar',
    'admin_update'=>'Admin taxrirlash',
    'delete'=>'O`chirish',
    'your_profile'=>'Sizning profiligiz',
    'read_only'=>'Faqat o`qish',
    'update'=>'Yangilash',
    'delete_message'=>'Ishonchingiz komilmi?',


    // Teachers //

    'balance'=>'Balans',
    'science'=>'Fan',

    // room.show //

    'room'=>'Xona',
    'monday' => 'Dushanba',
    'tuesday' => 'Seshanba',
    'wednesday' => 'Chorshanba',
    'thursday' => 'Payshanba',
    'friday' => 'Juma',
    'saturday' => 'Shanba',
    'sunday' => 'Yakshanba',

    'send_sms_all_students'=>'Barcha talabalarga sms yuborish',
    'sms'=>'SMS',
    'type_sms'=>'SMS kiriting',
    'send'=>'Jo`natish',
    'gender'=>'Jins',
    'birthday'=>'Tug`ilgan sana',
    'discount_education'=>'Chegirma',
    'parent_phone'=>'Ota-ona telefon raqami',
    'select_gender'=>'Jinsni tanlang',
    'male'=>'Erkak',
    'female'=>'Ayol',
    'type_parent_phone'=>'Ota-ona telefon raqami kiriting',
    'percent'=>'Foiz',
    'select_group'=>'Guruhni tanlang',
    'select_lid'=>'Lid tanlang',
    'comment'=>'Izoh',
    'type_comment'=>'Izoh kiriting',



    // Groups //

    'a_z'=>'A-z',
    'z_a'=>'Z-a',
    'absent'=>'Y',
    'reason'=>'S',
    'came'=>'B',

    'missed'=>'Qoldirilgan',

    'added_date'=>'Q`shilgan sana',
    'attendance'=>'Davomat',

    'level'=>'Daraja',
    'amount'=>'Miqdor',
    'teacher'=>'O`qituvchi',
    'course'=>'Kurslar',
    'start'=>'Boshlanish',
    'end'=>'Tugash',
    'type_level'=>'Daraja kiriting',
    'type_amount'=>'Miqdor kiriting',
    'not_selected'=>'Tanlanmagan',
    'teacher_percent'=>'O`qituvchi foizi',
    'add_graphics'=>'Grafik qo`shish',
    'select_year'=>'Yilni tanlang',
    'education'=>'Ta`lim',
    'kitchen'=>'Oshxona',
    'bedroom'=>'Yotoqxona',


    // Graphics //

    'group_graphic_title'=>'Ushbu guruhga SMS yuborish : ',
    'group_graphic_text'=>'Xabarlar tanlangan guruh a`zolariga yuboriladi',
    'unfinished'=>'Tugallanmagan',
    'finished'=>'Tugallangan',
    'student'=>'O`quvchi',
    'graph'=>'Grafik',
    'month'=>'Oy',
    'unpaid'=>'To`lanmagan',
    'send_sms'=>'SMS jo`natish',


    // cash box //

    'cash_cashbox'=>'Naqd',
    'card_cashbox'=>'Karta orqali',
    'click_cashbox'=>'Click orqali',
    'personal'=>'Shaxsiy',


    // payments //

    'date'=>'Sana',
    'discount'=>'Chegirma',
    'type_discount'=>'Chegirma kiritish',
    'worker'=>'Ishchi',
    'user'=>'Foydalanuvchi',





    // Controllers //

    'saved'=>'Muvaffaqiyatli saqlandi',
    'updated'=>'Muvaffaqiyatli yangilandi',
    'deleted'=>'Muvaffaqiyatli o`chirildi',
    'cannot_create'=>'Saqlab bo`lmadi',
    'cannot_update'=>'Yangilab bo`lmadi',
    'cannot_delete'=>'O`chirib bo`lmadi',

    'student_registered'=>'Talaba roʻyxatdan oʻtdi',

    'all_sms_sent'=>'Barcha SMS yuborildi.',

    'sms_sent'=>'SMS yuborildi.',

    'busy_room'=>'Xona tanlangan vaqtda band. Guruhni saqlab bo‘lmadi',

    'sms_updated'=>'Sms xizmat yangilandi',
    'sms_invalid'=>'Login yoki parol noto`g`ri',


    'limit'=>'Platformaga talabalarni qo`shish maksimal darajaga yetdi',


    'date_zone'=>'uz-uz',

    'text'=>'Text',
    'service'=>'Xizmat',
    'status'=>'Holat',

    'test'=>'Test',
    'result'=>'Natija',
    'type_result'=>'Natija kiriting',

    'reason_text'=>'Sababli',
    'absent_text'=>'Yo`q',

    'login_header'=>'Email va parol kiriting',
    'remember'=>'Eslab qolish',
    'login'=>'Kirish',
    'or'=>'YOKI',
    'forget_password'=>'Parolni unutdim',

    'kassa_error'=>'Kassada yetarli mablag` mavjud emas.',
    'teacher_balance_error'=>'O`qituvchi hisobida yetarli mablag` mavjud emas.',

    'daily_timetable'=>'Kunlik dars jadval',

    'upload_excel'=>'Excel fayl tanlang',


    'nickname'=>'Nickname',
    'key'=>'Key',
    'secret'=>'Secret',
    'token'=>'Token',













];
