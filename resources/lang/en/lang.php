<?php

return [
    // main page //
    'title' => 'Hello',
    'page'=>'Page',
    'sms_service'=>'SMS service',
    'email'=>'Email',
    'type_email'=>'Type email',
    'password'=>'Password',
    'type_password'=>'Type password',
    'checking'=>'Checking',
    'close'=>'Close',
    'save'=>'Save',
    'main_page'=>'Main page',
    'lids'=>'Lids',
    'days'=>'days',
    'expired'=>'Expired',
    'search'=>'Search',
    'english'=>'Eng',
    'uzbek'=>'Uz',
    'russian'=>'Ru',
    'karakalpak'=>'Qq',
    'page_here'=>'Page here',


    // sidebar //

    'dashboard'=>'Dashboard',
    'profile'=>'Profile',
    'admins'=>'Admins',
    'teachers'=>'Teachers',
    'workers'=>'Workers',
    'edit_profile'=>'Edit profile',
    'log_out'=>'Log out',
    'rooms'=>'Rooms',
    'sciences'=>'Sciences',
    'students'=>'Students',
    'courses'=>'Courses',
    'groups'=>'Groups',
    'graphics'=>'Graphics',
    'cashbox'=>'Cash box',
    'payments'=>'Payments',
    'outputs'=>'Outputs',
    'tests'=>'Tests',
    'developer'=>'Developer',


    // Dashboard //
    'more_info'=>'More info ',
    'payment_graphics'=>'Payment graphics',
    'calendar'=>'Calendar',
    'group'=>'Group',
    'paid'=>'Paid',
    'remaining'=>'Remaining',


    // Admins //
    'name'=>'Name',
    'phone'=>'Phone',
    'image'=>'Image',
    'role'=>'Role',
    'add'=>'Add',
    'type_name'=>'Type name',
    'type_phone'=>'Type phone',
    'select'=>'Select',
    'rights'=>'Rights',
    'admin_update'=>'Admin Update',
    'delete'=>'Delete',
    'your_profile'=>'Your profile',
    'read_only'=>'Read only',
    'update'=>'Update',
    'delete_message'=>'Are you sure?',


    // Teachers //

    'balance'=>'Balance',
    'science'=>'Science',

    // room.show //

    'room'=>'Room',
    'monday' => 'Monday',
    'tuesday' => 'Tuesday',
    'wednesday' => 'Wednesday',
    'thursday' => 'Thursday',
    'friday' => 'Friday',
    'saturday' => 'Saturday',
    'sunday' => 'Sunday',

    'send_sms_all_students'=>'Send sms all students',
    'sms'=>'SMS',
    'type_sms'=>'Write SMS',
    'send'=>'Send',
    'gender'=>'Gender',
    'birthday'=>'Birthday',
    'discount_education'=>'Discount',
    'parent_phone'=>'Parent phone',
    'select_gender'=>'Select gender',
    'male'=>'Male',
    'female'=>'Female',
    'type_parent_phone'=>'Type parent phone',
    'percent'=>'Percent',
    'select_group'=>'Select group',
    'select_lid'=>'Select lid',
    'comment'=>'Comment',
    'type_comment'=>'Type comment',



    // Groups //

    'a_z'=>'A-z',
    'z_a'=>'Z-a',
    'absent'=>'A',
    'reason'=>'R',
    'came'=>'C',

    'missed'=>'Missed',

    'added_date'=>'Added date',
    'attendance'=>'Attendance',

    'level'=>'Level',
    'amount'=>'Amount',
    'teacher'=>'Teacher',
    'course'=>'Course',
    'start'=>'Start',
    'end'=>'End',
    'type_level'=>'Type level',
    'type_amount'=>'Type amount',
    'not_selected'=>'Not Selected',
    'teacher_percent'=>'Teacher percent',
    'add_graphics'=>'Add graphics',
    'select_year'=>'Select year',
    'education'=>'Education',
    'kitchen'=>'Kitchen',
    'bedroom'=>'Bedroom',


    // Graphics //

    'group_graphic_title'=>'Send sms this group : ',
    'group_graphic_text'=>'Messages will be sent selected group members.',
    'unfinished'=>'Unfinished',
    'finished'=>'Finished',
    'student'=>'Student',
    'graph'=>'Graph',
    'month'=>'Month',
    'unpaid'=>'Unpaid',
    'send_sms'=>'Send SMS',


    // cash box //

    'cash_cashbox'=>'Cash box',
    'card_cashbox'=>'Card box',
    'click_cashbox'=>'Click box',
    'personal'=>'Personal',


    // payments //

    'date'=>'Date',
    'discount'=>'Discount',
    'type_discount'=>'Type discount',
    'worker'=>'Worker',
    'user'=>'User',





    // Controllers //

    'saved'=>'Saved successfully',
    'updated'=>'Updated successfully',
    'deleted'=>'Deleted successfully',
    'cannot_create'=>'Cannot be saved',
    'cannot_update'=>'Cannot be updated',
    'cannot_delete'=>'Cannot be deleted',

    'student_registered'=>'Student successfully registered',

    'all_sms_sent'=>'All SMS sent.',

    'sms_sent'=>'SMS has been sent.',

    'busy_room'=>'The room is busy in selected time. Group cannot be saved',

    'sms_updated'=>'Sms service updated',
    'sms_invalid'=>'Login or password invalid',


    'limit'=>'Platform reached maximal adding students',


    'date_zone'=>'en-en',

    'text'=>'Text',
    'service'=>'Service',
    'status'=>'Status',

    'test'=>'Test',
    'result'=>'Result',
    'type_result'=>'Type result',

    'reason_text'=>'Reasonable',
    'absent_text'=>'Absent',

    'login_header'=>'Enter email and password',
    'remember'=>'Remember',
    'login'=>'Login',
    'or'=>'OR',
    'forget_password'=>'Forgot password',

    'kassa_error'=>'Insufficient balance in cashbox',
    'teacher_balance_error'=>'Insufficient in teacher`s balance',

    'daily_timetable'=>'Daily timetable',

    'upload_excel'=>'Choose excel file',


    'nickname'=>'Nickname',
    'key'=>'Key',
    'secret'=>'Secret',
    'token'=>'Token',







];
