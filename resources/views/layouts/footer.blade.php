<footer class="main-footer">
    <strong>Copyright &copy; {{ date('Y').'-'.date('Y')+1 }}
        <a href="https://abdurahman.uz/" target="_blank">
            IslamAbdurahman
        </a>.
    </strong>
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.2.0
    </div>
</footer>
